@component('mail::message') 
<h1> Candidatura enviada!</h1>
<p>Olá {{ $usuario->name }}, sua candidatura foi confirmada!</p>
<p>Enviamos uma confirmação para: {{$usuario->email}}.</p>
@component('mail::button', ['url' => 'http://localhost/e-inov/public/vagas' ])
	Visualizar nossas vagas
@endcomponent
<p style="font-size: 10px;">Este HTML é apenas para visualização, configurar o envio no .env e remover este parágrafo</p>
@endcomponent