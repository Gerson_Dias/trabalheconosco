<?php

namespace App\Http\Controllers;

use App\Models\JobApply;
use App\Models\JobPosting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class JobApplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  String  $slug
     * @return \Illuminate\Http\Response
     */
    public function create(String $slug)
    {
        
        /* VALIDANDO QUE A VAGA EXISTE E ESTA DISPONIVEL */
        $validator = Validator::make(compact('slug'),[
            'slug' => [
                'required',
                Rule::exists('job_postings')->where(function ($query) use ($slug) {
                    return $query->where('slug', $slug)->where('valid_through','>=',now());
                }),
            ],
        ],[
            'slug.exists' => 'Que pena, está vaga já não está mais disponível.',
        ]);
        /* RETORNA PARA LISTA DE VAGAS */
        if ($validator->fails()) {
            return redirect(route('jobposting.index'))
                ->withErrors($validator);
        }
        /* PESQUISANDO OS DADOS DA VAGA */
        $jobposting = JobPosting::where('slug','=',$slug)->where('valid_through','>=',now())->first();
        
        //confere se ja existe candidatura
        $jobApply = JobApply::where('user_id','=',Auth::id())->where('job_posting_id','=',$jobposting->id)->first();
        
        //exclui caso exista
        if($jobApply){ 
            $this->destroy($jobApply, $jobposting);
            return view('jobapply.create',compact('jobposting'));

        }else{            
            return view('jobapply.create',compact('jobposting'));
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->file('curriculum'),$request->all());

        $aplication = new JobApply();
        $aplication->user_id = Auth::id();
        $aplication->job_posting_id = $request->id_job;
        $aplication->curriculum = $request->file('curriculum')->store('uploads/candidato_'.Auth::id());
        $aplication->challenge_date = $request->challenge_date;
        $aplication->salary_claim = intval(str_replace(".","", $request->salary_claim));
        $aplication->created_at = date('Y-m-d H:i:s');
        $aplication->updated_at = date('Y-m-d H:i:s');
        //$aplication->save();

        $user = User::where('id',Auth::id())->first();
        //dd($user);
        return new \App\Mail\sendApplication($user); //USAR AQUI PARA TESTES POR VIEW
        //Mail::send(new \App\Mail\sendApplication($user));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JobApply  $jobApply
     * @return \Illuminate\Http\Response
     */
    public function show(JobApply $jobApply)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JobApply  $jobApply
     * @return \Illuminate\Http\Response
     */
    public function edit(JobApply $jobApply)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JobApply  $jobApply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JobApply $jobApply)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JobApply  $jobApply
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobApply $jobApply, JobPosting $jobposting)
    {
        $ret=JobApply::where('id',$jobApply->id)->delete();
    }
}
